// App for testing a embedded devices via a serial port.
// This app it can be reads special script file, read and
// write text from serial port, check device response
// and notify user about a testing error.
//
// Usage:
//   serial-tester <script-file>
//
// Script file is a text file with commands and command parameters
// splitted by the space or the tab.
//
// Commands list:
//   open         "<port>" baudrate
//   echo         <on|off>
//   timeout      <timeout_in_ms>
//   waitfor      "<string>"
//   waitnewline
//   send         (<bs>|<enter>|"<text>")*
//   assert       "<regexp>"
//
// open "<port>" baudrate:
//   Open the serial port "port" with baud "baudrate".
//
// echo <on|off>:
//   On/off output of data which readed from the serial port.
//
// timeout <timeout_in_ms>:
//   Set timeout in the ms for all following read operations.
//
// waitfor "<string>"
//   Wait for the line "string" in the serial port input stream.
//
// waitnewline:
//   Wait new line in the input stream.
//
// send (<bs>|<enter>|"<text>")*
//   Send text "text" or special characters bs (backspace)
//   or enter to the serial port.
//
// assert "<regexp>"
//   Match a next line with "regexp" regilar expression.
//
// Example:
//   open "/dev/ttyS0" 115200
//   echo on
//   timeout 2000
//   waitfor "RedBoot> "
//
package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"

	"./serial"
)

// LineReader is a wrapper for the io.Reader. He can
// be read lines. Implements io.Reader.
type LineReader struct {
	reader io.Reader
}

// Read p bytes.
func (rd *LineReader) Read(p []byte) (int, error) {
	return rd.reader.Read(p)
}

// Read line as string. If EOF, function returns error
// and readed part of data. Next call returns empty string
// and error.
func (rd *LineReader) ReadLine() (string, error) {
	str := bytes.NewBuffer([]byte{})
	p := make([]byte, 1)

	for {
		_, err := rd.Read(p)
		if err != nil {
			return str.String(), err
		}

		if p[0] == '\n' {
			break
		}

		if p[0] != '\r' {
			str.Write(p)
		}
	}

	return str.String(), nil
}

// Constructor for the LineReader
func NewLineReader(r io.Reader) *LineReader {
	reader := new(LineReader)
	reader.reader = r

	return reader
}

// Wait for string from input stream and echoes data (if echo == true)
// to the output stream.
func waitForString(str string, rd io.Reader, wr io.Writer, echo bool) error {
	var err error
	bytes := make([]byte, 1)

	for n := 0; ; {
		if n >= len(str) {
			break
		}

		_, err = rd.Read(bytes)
		if err != nil {
			return err
		}

		if echo {
			wr.Write(bytes)
		}

		if str[n] == bytes[0] {
			n = n + 1
		} else {
			n = 0
		}
	}

	return nil
}

// Parameter of command.
type cmdParam struct {
	isString bool
	value    string
}

// Constructor for cmdParam.
func newParam(value string, isString bool) *cmdParam {
	param := new(cmdParam)
	param.value = value
	param.isString = isString

	return param
}

// Command
type scriptCmd struct {
	cmd    string
	params []cmdParam
}

// Check symbol "byte" is existed in string "delim"
func isDelim(b byte, delims string) bool {
	for i := 0; i < len(delims); i++ {
		if delims[i] == b {
			return true
		}
	}

	return false
}

// Parse command string (split string into a command and parameters).
func parseString(line string) *scriptCmd {
	str := bytes.NewBuffer([]byte{})
	cmd := new(scriptCmd)

	tok := false
	quoted := false
	first := true

	for n := 0; n < len(line); n++ {
		b := line[n]

		if b == '\r' {
			continue
		}

		if quoted {
			if b == '"' {
				if first {
					cmd.cmd = str.String()
					first = false
				} else {
					cmd.params = append(cmd.params, *newParam(str.String(), true))
				}
				quoted = false
				tok = false
				str.Reset()
			} else {
				str.WriteByte(b)
			}
		} else {
			if b == '#' {
				break
			} else {
				if isDelim(b, " \t\n") {
					if tok {
						if first {
							cmd.cmd = str.String()
							first = false
						} else {
							cmd.params = append(cmd.params, *newParam(str.String(), false))
						}
						quoted = false
						tok = false
						str.Reset()
					}
				} else {
					if tok {
						str.WriteByte(b)
					} else {
						if b == '"' {
							quoted = true
						} else {
							tok = true
							str.WriteByte(b)
						}
					}
				}
			}
		}
	}

	if str.Len() > 0 {
		if first {
			cmd.cmd = str.String()
		} else {
			cmd.params = append(cmd.params, *newParam(str.String(), quoted))
		}
	}

	return cmd
}

const TIMEOUT_DEFAULT = 10000

// Execute script
func doScript(script io.Reader) error {
	var com *serial.Port = nil
	var reader *LineReader

	echo := true

	sc := bufio.NewReader(script)

	for {
		line, err := sc.ReadString('\n')
		if err == io.EOF {
			break
		} else {
			if err != nil {
				return err
			}
		}

		cmd := parseString(line)

		/*
			fmt.Printf("CMD: '%s' ", cmd.cmd)
			for _, p := range cmd.params {
				fmt.Printf("'%s' ", p.value)
			}
			fmt.Println()
		*/

		if cmd.cmd == "open" {
			if len(cmd.params) != 2 {
				return errors.New("open must be have a string (name) and number (baudrate) as parameters")
			}
			if !cmd.params[0].isString {
				return errors.New("open must be have a string (name) as first parameter")
			}
			baud, err := strconv.Atoi(cmd.params[1].value)
			if err != nil {
				return errors.New("open must be have a number (baudrate) as second parameter")
			}

			com, err = serial.Open(cmd.params[0].value, serial.BaudParam(baud))
			if err != nil {
				return err
			}
			defer com.Close()

			reader = NewLineReader(com)
		} else {
			if cmd.cmd == "" {
				continue
			}

			if com == nil {
				return errors.New("as first open the com device (or file)")
			}

			switch cmd.cmd {
			case "echo":
				if len(cmd.params) != 1 {
					return errors.New("echo must be have a parameter [on|off]")
				}
				switch cmd.params[0].value {
				case "on":
					echo = true
				case "off":
					echo = false
				default:
					return errors.New("unknown parameter '" + cmd.params[0].value + "' in echo cmd")
				}

			case "timeout":
				if len(cmd.params) != 1 {
					return errors.New("timeout must be have a number of msecs as parameter")
				}
				timeout, err := strconv.Atoi(cmd.params[0].value)
				if err != nil {
					return err
				}

				com.SetTimeout(timeout)

			case "waitfor":
				if len(cmd.params) != 1 {
					return errors.New("waitfor must be have a string as parameter")
				}
				if !cmd.params[0].isString {
					return errors.New("waitfor must be have a string as parameter")
				}

				err = waitForString(cmd.params[0].value, reader, os.Stdout, echo)
				if err != nil {
					return err
				}

			case "waitnewline":
				if len(cmd.params) > 0 {
					return errors.New("waitnewline must not be have a parameters")
				}

				err = waitForString("\n", reader, os.Stdout, echo)
				if err != nil {
					return err
				}

			case "send":
				if len(cmd.params) < 1 {
					return errors.New("send must be have at least that one parameter")
				}

				for _, p := range cmd.params {
					if p.isString {
						_, err = io.WriteString(com, p.value)
						if err != nil {
							return err
						}
					} else {
						switch p.value {
						case "enter":
							_, err = io.WriteString(com, "\n")
							if err != nil {
								return err
							}
						case "bs":
							_, err = io.WriteString(com, "\b")
							if err != nil {
								return err
							}
						default:
							_, err = io.WriteString(com, p.value)
							if err != nil {
								return err
							}
						}
					}
				}

			case "assert":
				if len(cmd.params) != 1 {
					return errors.New("assert must be have a string as parameter")
				}
				if !cmd.params[0].isString {
					return errors.New("assert must be have a string as parameter")
				}

				rx, err := regexp.Compile(cmd.params[0].value)
				if err != nil {
					return err
				}

				line, err := reader.ReadLine()
				if err != nil {
					return err
				}

				if echo {
					fmt.Println(line)
				}

				if !rx.MatchString(line) {
					return errors.New("FAIL assertion '" + cmd.params[0].value + "'")
				}

			default:
				return errors.New("unknown command '" + cmd.cmd + "'")
			}
		}
	}

	return nil
}

// Main function
func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage:")
		fmt.Printf("    %s <script-file.test>\n\n", os.Args[0])
		return
	}

	file, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Print("\n")
		log.Fatal(err)
	}
	defer file.Close()

	err = doScript(file)
	if err != nil {
		fmt.Printf("\n---- TESTS FAIL: %s\n", err)
		return
	}

	fmt.Println("\n---- TESTS COMPLETE")
	return
}

/* vim: set ts=4 sts=4 sw=4: */
