/********************************************
 * Serial port driver
 * Source
 ********************************************/
#include <assert.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>

#include "serial.h"

struct SerialPort {
	int fd;
};

static unsigned int
get_time_ms( void )
{
    unsigned int time;
    struct timeval t;
    struct timezone tz;
    tz.tz_dsttime = 0;
    tz.tz_minuteswest = 0;

    gettimeofday( &t, &tz );
    time = t.tv_sec * 1000 + t.tv_usec / 1000;

    return time;
}

/**
 * Open serial port 'name' with parameters from 'params'
 * Returns: file descriptor or -1
 */
int
serial_open( struct SerialPort *port, const char* name, unsigned int params )
{
	assert( port );
	assert( name );

	int fd = open( name, O_RDWR );
	if( fd == -1 )
		return -E_SERIAL_OPEN;
    
	struct termios tio;
	if( tcgetattr(fd, &tio) ) {
		close( fd );
		return -E_SERIAL_GETATTRS;
	}

	tcflag_t speed;
	switch (params & SER_BAUD_MASK) {
		case SER_BAUD_600:	speed = B600; break;
		case SER_BAUD_1200:	speed = B1200; break;
		case SER_BAUD_2400:	speed = B2400; break;
		case SER_BAUD_4800:	speed = B4800; break;
		case SER_BAUD_9600:	speed = B9600; break;
		case SER_BAUD_19200:	speed = B19200; break;
		case SER_BAUD_38400:	speed = B38400; break;
		case SER_BAUD_57600:	speed = B57600; break;
		case SER_BAUD_115200:	speed = B115200; break;
		default:
			return -E_SERIAL_SETATTRS;
	}

	tio.c_cflag = speed | CS8; 
	tio.c_cflag |= CREAD | CLOCAL;
	tio.c_iflag = 0;
	tio.c_oflag = 0;
	tio.c_lflag = 0;

	/* switch off wait */
	tio.c_cc[VMIN] = 0;
	tio.c_cc[VTIME] = 0;
	
	tcflush( fd, TCIFLUSH );
	if( tcsetattr( fd, TCSANOW, &tio ) ) {
		close( fd );
		return -E_SERIAL_SETATTRS;
	}
    
	port->fd = fd;

	return 0;
}


int
serial_close( struct SerialPort *port )
{
	assert( port );

	if( close( port->fd ) != 0 )
		return -E_SERIAL_CLOSE; 

	return 0;
}

int
serial_wrbyte( struct SerialPort *port, unsigned char byte )
{
	assert( port );

	//printf( "0x%.2x\n", byte );

	unsigned char b = byte;
	if( write( port->fd, &b, 1 ) < 1 )
		return -E_SERIAL_WRITE;
	
	return 0;
}

int
serial_wrbuf( struct SerialPort *port, unsigned char *buf, unsigned int len )
{
	assert( port );

	if( write( port->fd, buf, len ) < len )
		return -E_SERIAL_WRITE;
	
	return 0;
}

int
serial_rdbyte( struct SerialPort *port, unsigned char* retval, int tout )
{
	assert( port );

	int r;
	unsigned char tmp;

	if( !retval ) {
		retval = &tmp;
	}

	if( tout == 0 ) {

		do {
			r = read( port->fd, retval, 1 );
			if( r == 0 )
				usleep( 1000 );
		} while( r == 0 );
	
	} else
	if( tout > 0 ) {
	
		unsigned int to = get_time_ms() + tout;
		do {
			r = read( port->fd, retval, 1 );
			if( r == 0 ) {
				if( get_time_ms() >= to )
					return -E_SERIAL_TIMEOUT;
				else
					usleep( 1000 );
			}
		} while( r == 0 );

	} else
		r = read( port->fd, retval, 1 );

	//if( r > 0 )
	//	printf( "rd 0x%.2x\n", *retval );

	if( r < 0 )
		return -E_SERIAL_READ;

	return r;
}

struct SerialPort *
new_SerialPort()
{
	return (struct SerialPort*) malloc(sizeof (struct SerialPort));
}

void
delete_SerialPort(struct SerialPort *port)
{
	free(port);
}

void
serial_flush(struct SerialPort *port)
{
	while( serial_rdbyte( port, NULL, -1 ) == 1 );
}
