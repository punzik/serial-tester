#include <windows.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "serial.h"

struct SerialPort {
	HANDLE hndl;
};

int
serial_open( struct SerialPort *port, const char* name, unsigned int params )
{
	HANDLE hndl;
	DCB attrs = { 0 };
	COMMTIMEOUTS timeouts = { 0 };

	assert( port );

	hndl = CreateFile( name, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0 );
	if( hndl == INVALID_HANDLE_VALUE )
		return -E_SERIAL_OPEN;

	attrs.DCBlength = sizeof( DCB );
	
	if( !GetCommState( hndl, &attrs ) )
		return -E_SERIAL_GETATTRS;

	switch (params & SER_BAUD_MASK) {
		case SER_BAUD_600:	attrs.BaudRate = CBR_600; break;
		case SER_BAUD_1200:	attrs.BaudRate = CBR_1200; break;
		case SER_BAUD_2400:	attrs.BaudRate = CBR_2400; break;
		case SER_BAUD_4800:	attrs.BaudRate = CBR_4800; break;
		case SER_BAUD_9600:	attrs.BaudRate = CBR_9600; break;
		case SER_BAUD_19200:	attrs.BaudRate = CBR_19200; break;
		case SER_BAUD_38400:	attrs.BaudRate = CBR_38400; break;
		case SER_BAUD_57600:	attrs.BaudRate = CBR_57600; break;
		case SER_BAUD_115200:	attrs.BaudRate = CBR_115200; break;
		default:
			return -E_SERIAL_SETATTRS;
	}

	attrs.ByteSize = 8;
	attrs.StopBits = ONESTOPBIT;
	attrs.Parity = NOPARITY;
	
	if( !SetCommState( hndl, &attrs ) )
		return -E_SERIAL_SETATTRS;

	timeouts.ReadIntervalTimeout = MAXDWORD;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 10;
	timeouts.WriteTotalTimeoutMultiplier = 10;
	if( !SetCommTimeouts( hndl, &timeouts ) )
		return -E_SERIAL_SETATTRS;

	port->hndl = hndl;

	return 0;
}

int
serial_close( struct SerialPort *port )
{
	assert( port );

	if( CloseHandle( port->hndl ) == 0 )
		return -E_SERIAL_CLOSE;
	
	return 0;
}

int
serial_wrbuf( struct SerialPort *port, unsigned char *buf, unsigned int len )
{
	assert( port );

	if( WriteFile( port->hndl, buf, len, NULL, NULL ) == 0 )
		return -E_SERIAL_WRITE;
	
	return 0;
}

int
serial_wrbyte( struct SerialPort *port, unsigned char byte )
{
	DWORD w = 0;

	assert( port );

	if( WriteFile( port->hndl, &byte, 1, &w, NULL ) == 0 )
		return -E_SERIAL_WRITE;
	
	return 0;
}

int
serial_rdbyte( struct SerialPort *port, unsigned char* retval, int tout )
{
	unsigned char buf[2];
	DWORD r = 0, to;

	assert( port );

	if( tout == 0 ) {

		do {
			if( ReadFile( port->hndl, buf, 1, &r, NULL) == 0 )
				return -E_SERIAL_READ;
			if( r == 0 )
				Sleep( 1 );
		} while( r == 0 );
	
	} else
	if( tout > 0 ) {
	
		to = timeGetTime() + tout;
		do {
			if( ReadFile( port->hndl, buf, 1, &r, NULL) == 0 )
				return -E_SERIAL_READ;
			if( r == 0 ) {
				if( timeGetTime() >= to )
					return -E_SERIAL_TIMEOUT;
				else
					Sleep( 1 );
			}
		} while( r == 0 );

	} else
		if( ReadFile( port->hndl, buf, 1, &r, NULL) == 0 )
			return -E_SERIAL_READ;
	
	if( retval )
		*retval = buf[0];

	return r;
}

struct SerialPort *
new_SerialPort()
{
	return (struct SerialPort*) malloc(sizeof (struct SerialPort));
}

void
delete_SerialPort(struct SerialPort *port)
{
	free(port);
}

void
serial_flush(struct SerialPort *port)
{
	while( serial_rdbyte( port, NULL, -1 ) == 1 );
}
