// Package serial provided crossplatform interface to the serial port.
package serial

// #cgo windows LDFLAGS: -lwinmm
// #include <stdlib.h>
// #include "serial.h"
import "C"

import (
	"errors"
	"unsafe"
)

// Baud rates for serial.Open
const (
	BAUD_600     = 0x0001
	BAUD_1200    = 0x0002
	BAUD_2400    = 0x0003
	BAUD_4800    = 0x0004
	BAUD_9600    = 0x0005
	BAUD_19200   = 0x0006
	BAUD_38400   = 0x0007
	BAUD_57600   = 0x0008
	BAUD_115200  = 0x0009
	BAUD_DEFAULT = BAUD_115200
)

// Converts numerical baud rate to code for Open function.
func BaudParam(baud int) int {
	switch baud {
	case 600:
		return BAUD_600
	case 1200:
		return BAUD_1200
	case 2400:
		return BAUD_2400
	case 4800:
		return BAUD_4800
	case 9600:
		return BAUD_9600
	case 19200:
		return BAUD_19200
	case 38400:
		return BAUD_38400
	case 57600:
		return BAUD_57600
	case 115200:
		return BAUD_115200
	}

	return BAUD_DEFAULT
}

/** C Functions
 *
 * int serial_open( struct SerialPort *port, const char* name, unsigned int params );
 * int serial_close( struct SerialPort *port );
 * int serial_wrbyte( struct SerialPort *port, unsigned char byte );
 * int serial_wrbuf( struct SerialPort *port, unsigned char *buf, unsigned int len );
 * int serial_rdbyte( struct SerialPort *port, unsigned char* retval, int tout );
 * void serial_flush( struct SerialPort *port );
 * struct SerialPort *new_SerialPort();
 * void delete_SerialPort(struct SerialPort *port);
 */

// Port is struct with private serial port data.
// Implements io.ReaderWriter interface
type Port struct {
	port    *C.struct_SerialPort
	timeout C.int
}

// Open serial port name with parameters params.
// Name sould be the character device name in linux (e.g /dev/ttyS0)
// or logical port name in windows (e.g COM1).
// 
// Open returns pointer to Port struct and error
func Open(name string, params int) (*Port, error) {
	var err C.int
	var port Port

	c_name := C.CString(name)
	defer C.free(unsafe.Pointer(c_name))

	port.port = C.new_SerialPort()

	err = C.serial_open(port.port, c_name, C.uint(params))
	if int(err) != 0 {
		C.delete_SerialPort(port.port)
		return nil, errors.New(C.GoString(C.error_message(err)))
	}

	port.timeout = C.int(0)

	return &port, nil
}

// Closes serial port
func (port *Port) Close() {
	C.serial_flush(port.port)
	C.serial_close(port.port)
}

// Set timeout for read operation in ms.
// If timeout == 0, Read function wait data on port
// as long as data receive.
// If timeout < 0, Read do not wait data and return
// immediately.
func (port *Port) SetTimeout(timeout int) {
	port.timeout = C.int(timeout)
}

// Read data from serial port.
// Read returns number of bytes really readed from port.
// If timeout < 0, then Read return 0 and no error when
// no data in port. Otherwise Read returns (>0, nill)
// or (0, error).
// Error can be a timeout.
func (port *Port) Read(p []byte) (int, error) {
	var err C.int
	var b C.uchar

	readed := 0

	for i := 0; i < len(p); i++ {
		err = C.serial_rdbyte(port.port, &b, port.timeout)
		if int(err) < 0 {
			return readed, errors.New(C.GoString(C.error_message(err)))
		}

		if int(err) == 0 {
			return readed, errors.New("Cannot read from serial port")
		}

		p[i] = byte(b)
		readed++
	}

	if readed < len(p) {
		return readed, errors.New("Unknown error on read from serial port")
	}

	return readed, nil
}

// Write bytes to serial port.
// Returns number of bytes really written or error.
func (port *Port) Write(p []byte) (int, error) {
	var err C.int
	var b C.uchar

	writed := 0

	for i := 0; i < len(p); i++ {
		b = C.uchar(p[i])
		err = C.serial_wrbyte(port.port, b)

		if int(err) != 0 {
			return writed, errors.New(C.GoString(C.error_message(err)))
		}

		writed++
	}

	if writed < len(p) {
		return writed, errors.New("Unknown error on write to serial port")
	}

	return writed, nil

}

// Write all data from serial port OS buffer to port.
func (port *Port) Flush() {
	C.serial_flush(port.port)
}

/* vim: set ts=4 sts=4 sw=4: */
