#ifndef _SERIAL_H
#define _SERIAL_H

/**
 * Serial port parameters mask. Use in serial_open(,,params).
 */
#define SER_BAUD_ERR		0x0000
#define SER_BAUD_600		0x0001
#define SER_BAUD_1200		0x0002
#define SER_BAUD_2400		0x0003
#define SER_BAUD_4800		0x0004
#define SER_BAUD_9600		0x0005
#define SER_BAUD_19200		0x0006
#define SER_BAUD_38400		0x0007
#define SER_BAUD_57600		0x0008
#define SER_BAUD_115200		0x0009
#define SER_BAUD_MASK		0x000f

#define SER_BYTESIZE_8		0x0000
#define SER_BYTESIZE_7		0x0010
#define SER_BYTESIZE_6		0x0020
#define SER_BYTESIZE_5		0x0030
#define SER_BYTESIZE_MASK	0x0030

#define SER_PARITY_NONE		0x0000
#define SER_PARITY_ODD		0x0040
#define SER_PARITY_EVEN		0x0080
#define SER_PARITY_MASK		0x00c0

#define SER_STOPBIT_ONE		0x0000
#define SER_STOPBIT_TWO		0x0100
#define SER_STOPBIT_MASK	0x0100

/**
 * Errors
 */
#define E_SERIAL_OPEN		1100
#define E_SERIAL_GETATTRS	1101
#define E_SERIAL_SETATTRS	1102
#define E_SERIAL_CLOSE		1103
#define E_SERIAL_WRITE		1104
#define E_SERIAL_READ		1105
#define E_SERIAL_TIMEOUT	1106

static const char*
error_message( int err )
{
	if( err >= 0 ) {
		return "Unknown positive return code\n";
	}

	switch( err ) {
		case -E_SERIAL_OPEN:		return "Cannot open serial port";
		case -E_SERIAL_GETATTRS:	return "Cannot get serial port attributes";
		case -E_SERIAL_SETATTRS:	return "Cannot set serial port attributes";
		case -E_SERIAL_CLOSE:		return "Error on close serial port";
		case -E_SERIAL_WRITE:		return "Cannot write to serial port";
		case -E_SERIAL_READ:		return "Cannot read from serial port";
		case -E_SERIAL_TIMEOUT:		return "Serial port timeout";
	}

	return "Unknown error";
}

struct SerialPort;

/**
 * Open the serial port.
 *
 * @param port		pointer to SerialPort structure;
 * @param name		serial port path. E.g. "/dev/ttyS0" or "COM1";
 * @param params	serial port attributes mask.
 * @return		0 if ok, negative error code if fail.
 */
int serial_open( struct SerialPort *port, const char* name, unsigned int params );

/**
 * Close the serial port.
 */
int serial_close( struct SerialPort *port );

/**
 * Write byte to the serial port.
 */
int serial_wrbyte( struct SerialPort *port, unsigned char byte );

/**
 * Write buffer to the serial port.
 */
int serial_wrbuf( struct SerialPort *port, unsigned char *buf, unsigned int len );

/**
 * Read byte from serial port.
 *
 * @param port		pointer to the SerialPort structure;
 * @param retval	pointer to the return var. Maybe NULL;
 * @param tout		timeout in ms. if tout is 0, then timeout is infinite, if
 *			tout < 0, then no wait if nothing data for read.
 * @return		1 if byte readed, 0 if noting readed, negative error code if fail.
 */
int serial_rdbyte( struct SerialPort *port, unsigned char* retval, int tout );

/**
 * Golang helper function.
 * Create struct SerialPort
 */
struct SerialPort *new_SerialPort();

/**
 * Golang helper function.
 * Delete struct SerialPort
 */
void delete_SerialPort(struct SerialPort *port);

/**
 * Write all data from buffer to serial port
 */
void serial_flush(struct SerialPort *port);

#endif //_SERIAL_H
